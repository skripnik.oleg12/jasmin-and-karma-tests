const WLANS = wlans.map(w => Wlan.createFromObject(w));

class DialogServiceMock extends MockDialogObservableService {
}


describe('WlanComponent', () => {
    let component: WlanComponent;
    let fixture: ComponentFixture<WlanComponent>;
    let debugElem: DebugElement;
    let service: WlanService;
    let dialog: any;
    let confirmationService: ConfirmationService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                WlanComponent,
                WlanUpdateComponent
            ],
            providers: [
                BreadcrumbService,
                MessageService,
                ConfirmationService,
                {provide: DialogService, useClass: DialogServiceMock},
            ],
            imports: [
                PrimeNgModule,
                BrowserTestingModule,
                BrowserDynamicTestingModule,
                HttpClientTestingModule,
                DataTableModule
            ],
            schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(WlanComponent);
        component = fixture.componentInstance;
        service = TestBed.inject(WlanService);
        dialog = TestBed.inject(DialogService);
        confirmationService = TestBed.inject(ConfirmationService);
        debugElem = fixture.debugElement;

        spyOn(service, 'list').and.returnValue(of(WLANS));
        spyOn(service, 'delete').and.returnValue(of());
        fixture.detectChanges();

        expect(component.wlans).toEqual(WLANS);
        expect(component.wlans.length).toBe(WLANS.length);
    });

    afterEach(() => fixture.destroy());

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should unsubscribe after destroy', () => {
        spyOn(component.subscriptions, 'unsubscribe');
        fixture.destroy();

        expect(component.subscriptions.unsubscribe).toHaveBeenCalled();
    });

    it('should open create modal', async(() => {
        spyOn(component, 'saveItem');
        const button = debugElem.nativeElement.querySelector(`button#create`);
        button.click();
        fixture.detectChanges();

        const config = DialogTest.getDialogConfig(new Wlan());

        spyOn(dialog, 'open').and.returnValue(DialogServiceMock);
        dialog.open(WlanUpdateComponent, config);

        expect(component.saveItem).toHaveBeenCalled();
        expect(config.data).toBeTruthy();
        expect(config.data.id).toBeUndefined();
        expect(dialog.open).toHaveBeenCalledWith(WlanUpdateComponent, config);
    }));

    it('should open delete', async(() => {
        spyOn(component, 'deleteItem');

        const button = debugElem.nativeElement.querySelector(`button#delete-${component.wlans[0].id}`);
        button.click();
        fixture.detectChanges();

        expect(button).toBeTruthy();
        expect(component.deleteItem).toHaveBeenCalledWith(component.wlans[0]);
    }));
